<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="no" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="no" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Cartouche" color="54" fill="1" visible="yes" active="yes"/>
<layer number="101" name="DiagFonc" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="1_TGE-sur G">
<description>Bibliothèque de base pour les élèves de première année en TGÉ.</description>
<packages>
<package name="CARTOUCHE">
<wire x1="0" y1="0" x2="0" y2="42.418" width="0.127" layer="100"/>
<wire x1="0" y1="42.418" x2="0" y2="84.836" width="0.127" layer="100"/>
<wire x1="0" y1="84.836" x2="0" y2="127.254" width="0.127" layer="100"/>
<wire x1="0" y1="127.254" x2="0" y2="169.672" width="0.127" layer="100"/>
<wire x1="0" y1="169.672" x2="0" y2="209.55" width="0.127" layer="100"/>
<wire x1="0" y1="209.55" x2="49.53" y2="209.55" width="0.127" layer="100"/>
<wire x1="49.53" y1="209.55" x2="95.25" y2="209.55" width="0.127" layer="100"/>
<wire x1="95.25" y1="209.55" x2="140.97" y2="209.55" width="0.127" layer="100"/>
<wire x1="140.97" y1="209.55" x2="186.69" y2="209.55" width="0.127" layer="100"/>
<wire x1="186.69" y1="209.55" x2="232.41" y2="209.55" width="0.127" layer="100"/>
<wire x1="232.41" y1="209.55" x2="281.94" y2="209.55" width="0.127" layer="100"/>
<wire x1="281.94" y1="209.55" x2="281.94" y2="169.672" width="0.127" layer="100"/>
<wire x1="281.94" y1="169.672" x2="281.94" y2="127.254" width="0.127" layer="100"/>
<wire x1="281.94" y1="127.254" x2="281.94" y2="84.836" width="0.127" layer="100"/>
<wire x1="281.94" y1="84.836" x2="281.94" y2="42.418" width="0.127" layer="100"/>
<wire x1="281.94" y1="42.418" x2="281.94" y2="0" width="0.127" layer="100"/>
<wire x1="281.94" y1="0" x2="278.13" y2="0" width="0.127" layer="100"/>
<wire x1="278.13" y1="0" x2="257.556" y2="0" width="0.127" layer="100"/>
<wire x1="257.556" y1="0" x2="191.516" y2="0" width="0.127" layer="100"/>
<wire x1="191.516" y1="0" x2="3.81" y2="0" width="0.127" layer="100"/>
<wire x1="3.81" y1="0" x2="0" y2="0" width="0.127" layer="100"/>
<wire x1="3.81" y1="0" x2="3.81" y2="42.418" width="0.127" layer="100"/>
<wire x1="3.81" y1="42.418" x2="3.81" y2="84.836" width="0.127" layer="100"/>
<wire x1="3.81" y1="84.836" x2="3.81" y2="127.254" width="0.127" layer="100"/>
<wire x1="3.81" y1="127.254" x2="3.81" y2="169.672" width="0.127" layer="100"/>
<wire x1="3.81" y1="205.74" x2="49.53" y2="205.74" width="0.127" layer="100"/>
<wire x1="49.53" y1="205.74" x2="95.25" y2="205.74" width="0.127" layer="100"/>
<wire x1="95.25" y1="205.74" x2="140.97" y2="205.74" width="0.127" layer="100"/>
<wire x1="140.97" y1="205.74" x2="186.69" y2="205.74" width="0.127" layer="100"/>
<wire x1="186.69" y1="205.74" x2="232.41" y2="205.74" width="0.127" layer="100"/>
<wire x1="232.41" y1="205.74" x2="278.13" y2="205.74" width="0.127" layer="100"/>
<wire x1="278.13" y1="205.74" x2="278.13" y2="169.672" width="0.127" layer="100"/>
<wire x1="278.13" y1="169.672" x2="278.13" y2="127.254" width="0.127" layer="100"/>
<wire x1="278.13" y1="127.254" x2="278.13" y2="84.836" width="0.127" layer="100"/>
<wire x1="278.13" y1="84.836" x2="278.13" y2="42.418" width="0.127" layer="100"/>
<wire x1="278.13" y1="42.418" x2="278.13" y2="35.814" width="0.127" layer="100"/>
<wire x1="278.13" y1="35.814" x2="278.13" y2="32.004" width="0.127" layer="100"/>
<wire x1="278.13" y1="32.004" x2="278.13" y2="20.574" width="0.127" layer="100"/>
<wire x1="278.13" y1="20.574" x2="278.13" y2="12.954" width="0.127" layer="100"/>
<wire x1="278.13" y1="12.954" x2="278.13" y2="5.08" width="0.127" layer="100"/>
<wire x1="278.13" y1="5.08" x2="278.13" y2="0" width="0.127" layer="100"/>
<wire x1="281.94" y1="42.418" x2="278.13" y2="42.418" width="0.127" layer="100"/>
<wire x1="281.94" y1="84.836" x2="278.13" y2="84.836" width="0.127" layer="100"/>
<wire x1="281.94" y1="127.254" x2="278.13" y2="127.254" width="0.127" layer="100"/>
<wire x1="281.94" y1="169.672" x2="278.13" y2="169.672" width="0.127" layer="100"/>
<wire x1="3.81" y1="42.418" x2="0" y2="42.418" width="0.127" layer="100"/>
<wire x1="0" y1="84.836" x2="3.81" y2="84.836" width="0.127" layer="100"/>
<wire x1="3.81" y1="127.254" x2="0" y2="127.254" width="0.127" layer="100"/>
<wire x1="0" y1="169.672" x2="3.81" y2="169.672" width="0.127" layer="100"/>
<wire x1="49.53" y1="209.55" x2="49.53" y2="205.74" width="0.127" layer="100"/>
<wire x1="95.25" y1="209.55" x2="95.25" y2="205.74" width="0.127" layer="100"/>
<wire x1="140.97" y1="209.55" x2="140.97" y2="205.74" width="0.127" layer="100"/>
<wire x1="186.69" y1="209.55" x2="186.69" y2="205.74" width="0.127" layer="100"/>
<wire x1="232.41" y1="209.55" x2="232.41" y2="205.74" width="0.127" layer="100"/>
<text x="1.27" y="148.463" size="1.27" layer="100">B</text>
<wire x1="3.81" y1="169.672" x2="3.81" y2="205.74" width="0.127" layer="100"/>
<text x="1.27" y="106.045" size="1.27" layer="100">C</text>
<text x="1.27" y="63.627" size="1.27" layer="100">A</text>
<text x="1.27" y="21.209" size="1.27" layer="100">E</text>
<text x="1.27" y="190.881" size="1.27" layer="100">A</text>
<text x="279.527" y="148.463" size="1.27" layer="100">B</text>
<text x="279.527" y="106.045" size="1.27" layer="100">C</text>
<text x="279.527" y="63.627" size="1.27" layer="100">A</text>
<text x="279.527" y="21.209" size="1.27" layer="100">E</text>
<text x="279.527" y="190.881" size="1.27" layer="100">A</text>
<text x="26.67" y="207.01" size="1.27" layer="100">1</text>
<text x="72.39" y="207.01" size="1.27" layer="100">2</text>
<text x="118.11" y="207.01" size="1.27" layer="100">3</text>
<text x="163.83" y="207.01" size="1.27" layer="100">4</text>
<text x="209.55" y="207.01" size="1.27" layer="100">1</text>
<text x="255.27" y="207.01" size="1.27" layer="100">6</text>
<wire x1="191.516" y1="0" x2="191.516" y2="5.08" width="0.127" layer="100"/>
<wire x1="191.516" y1="5.08" x2="191.516" y2="12.954" width="0.127" layer="100"/>
<wire x1="191.516" y1="12.954" x2="191.516" y2="20.574" width="0.127" layer="100"/>
<wire x1="191.516" y1="20.574" x2="191.516" y2="28.194" width="0.127" layer="100"/>
<wire x1="191.516" y1="28.194" x2="191.516" y2="35.814" width="0.127" layer="100"/>
<wire x1="191.516" y1="35.814" x2="219.456" y2="35.814" width="0.127" layer="100"/>
<wire x1="219.456" y1="35.814" x2="278.13" y2="35.814" width="0.127" layer="100"/>
<wire x1="191.516" y1="5.08" x2="198.374" y2="5.08" width="0.127" layer="100"/>
<wire x1="198.374" y1="5.08" x2="208.28" y2="5.08" width="0.127" layer="100"/>
<wire x1="208.28" y1="5.08" x2="219.456" y2="5.08" width="0.127" layer="100"/>
<wire x1="219.456" y1="5.08" x2="257.556" y2="5.08" width="0.127" layer="100"/>
<wire x1="257.556" y1="5.08" x2="278.13" y2="5.08" width="0.127" layer="100"/>
<wire x1="191.516" y1="12.954" x2="198.374" y2="12.954" width="0.127" layer="100"/>
<wire x1="198.374" y1="12.954" x2="208.28" y2="12.954" width="0.127" layer="100"/>
<wire x1="208.28" y1="12.954" x2="278.13" y2="12.954" width="0.127" layer="100"/>
<wire x1="191.516" y1="20.574" x2="278.13" y2="20.574" width="0.127" layer="100"/>
<wire x1="191.516" y1="28.194" x2="219.456" y2="28.194" width="0.127" layer="100"/>
<wire x1="198.374" y1="5.08" x2="198.374" y2="12.954" width="0.127" layer="100"/>
<wire x1="208.28" y1="5.08" x2="208.28" y2="12.954" width="0.127" layer="100"/>
<wire x1="219.456" y1="5.08" x2="219.456" y2="28.194" width="0.127" layer="100"/>
<wire x1="219.456" y1="28.194" x2="219.456" y2="32.004" width="0.127" layer="100"/>
<wire x1="219.456" y1="32.004" x2="219.456" y2="35.814" width="0.127" layer="100"/>
<wire x1="257.556" y1="5.08" x2="257.556" y2="0" width="0.127" layer="100"/>
<wire x1="278.13" y1="32.004" x2="219.456" y2="32.004" width="0.127" layer="100"/>
<text x="192.024" y="33.782" size="1.27" layer="100">Dessiné par:</text>
<text x="192.024" y="26.162" size="1.27" layer="100">Vérifié par:</text>
<text x="192.278" y="18.542" size="1.27" layer="100">Approuvé par:</text>
<text x="192.024" y="10.922" size="1.27" layer="100">Format</text>
<text x="200.152" y="10.922" size="1.27" layer="100">Version</text>
<text x="210.566" y="10.922" size="1.27" layer="100">Révision</text>
<text x="220.218" y="10.922" size="1.27" layer="100">Titre du schématique:</text>
<text x="219.964" y="18.542" size="1.27" layer="100">Chemin et nom du design</text>
<text x="219.964" y="20.828" size="2.1844" layer="100" distance="25">CÉGEP de l'Outaouais
820 Boulevard La Gappe
Gatineau, Québec
J8T 7T7</text>
<text x="259.588" y="1.778" size="1.27" layer="100">Feuilles(s)      de</text>
<text x="192.024" y="3.048" size="1.27" layer="100">Date:</text>
<text x="220.98" y="6.35" size="2.54" layer="100" font="vector">&gt;DRAWING_NAME</text>
<text x="196.85" y="1.27" size="2.54" layer="100" font="vector">&gt;LAST_DATE_TIME</text>
</package>
</packages>
<symbols>
<symbol name="CADRE_FORMAT_A">
<frame x1="0" y1="0" x2="279.4" y2="215.9" columns="6" rows="5" layer="100" border-bottom="no"/>
</symbol>
<symbol name="DOCUMENTATION">
<wire x1="0" y1="0" x2="66.04" y2="0" width="0.1016" layer="100"/>
<wire x1="66.04" y1="0" x2="84.074" y2="0" width="0.1016" layer="100"/>
<wire x1="84.074" y1="35.56" x2="27.94" y2="35.56" width="0.1016" layer="100"/>
<wire x1="27.94" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="100"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="100"/>
<wire x1="0" y1="5.08" x2="0" y2="12.7" width="0.1016" layer="100"/>
<wire x1="0" y1="12.7" x2="0" y2="20.32" width="0.1016" layer="100"/>
<wire x1="0" y1="20.32" x2="0" y2="27.94" width="0.1016" layer="100"/>
<wire x1="0" y1="27.94" x2="0" y2="35.56" width="0.1016" layer="100"/>
<wire x1="84.074" y1="35.56" x2="84.074" y2="31.75" width="0.1016" layer="100"/>
<wire x1="84.074" y1="31.75" x2="84.074" y2="20.32" width="0.1016" layer="100"/>
<wire x1="84.074" y1="20.32" x2="84.074" y2="12.7" width="0.1016" layer="100"/>
<wire x1="84.074" y1="12.7" x2="84.074" y2="5.08" width="0.1016" layer="100"/>
<wire x1="84.074" y1="5.08" x2="84.074" y2="0" width="0.1016" layer="100"/>
<wire x1="27.94" y1="35.56" x2="27.94" y2="31.75" width="0.1016" layer="100"/>
<wire x1="27.94" y1="31.75" x2="27.94" y2="27.94" width="0.1016" layer="100"/>
<wire x1="27.94" y1="27.94" x2="27.94" y2="20.32" width="0.1016" layer="100"/>
<wire x1="0" y1="5.08" x2="6.858" y2="5.08" width="0.1016" layer="100"/>
<wire x1="6.858" y1="5.08" x2="16.764" y2="5.08" width="0.1016" layer="100"/>
<wire x1="16.764" y1="5.08" x2="27.94" y2="5.08" width="0.1016" layer="100"/>
<wire x1="27.94" y1="5.08" x2="66.04" y2="5.08" width="0.1016" layer="100"/>
<wire x1="66.04" y1="5.08" x2="84.074" y2="5.08" width="0.1016" layer="100"/>
<wire x1="0" y1="12.7" x2="6.858" y2="12.7" width="0.1016" layer="100"/>
<wire x1="6.858" y1="12.7" x2="16.764" y2="12.7" width="0.1016" layer="100"/>
<wire x1="0" y1="20.32" x2="27.94" y2="20.32" width="0.1016" layer="100"/>
<wire x1="0" y1="27.94" x2="27.94" y2="27.94" width="0.1016" layer="100"/>
<wire x1="6.858" y1="12.7" x2="6.858" y2="5.08" width="0.1016" layer="100"/>
<wire x1="16.764" y1="12.7" x2="84.074" y2="12.7" width="0.1016" layer="100"/>
<wire x1="16.764" y1="12.7" x2="16.764" y2="5.08" width="0.1016" layer="100"/>
<wire x1="27.94" y1="31.75" x2="84.074" y2="31.75" width="0.1016" layer="100"/>
<wire x1="27.94" y1="20.32" x2="84.074" y2="20.32" width="0.1016" layer="100"/>
<wire x1="27.94" y1="20.32" x2="27.94" y2="5.08" width="0.1016" layer="100"/>
<wire x1="66.04" y1="5.08" x2="66.04" y2="0" width="0.1016" layer="100"/>
<text x="7.366" y="1.778" size="2.54" layer="100" font="vector">&gt;LAST_DATE_TIME</text>
<text x="29.21" y="6.858" size="2.54" layer="100" font="vector">&gt;DRAWING_NAME</text>
<text x="28.702" y="28.702" size="2.1844" layer="100">CÉGEP de l'Outaouais</text>
<text x="28.702" y="26.162" size="1.778" layer="100">820 Boulevard La Gappe</text>
<text x="28.702" y="23.622" size="1.778" layer="100">Gatineau, Québec</text>
<text x="28.448" y="21.082" size="1.778" layer="100">J8T 7T7</text>
<text x="0.508" y="33.528" size="1.27" layer="100">Dessiné par:</text>
<text x="0.508" y="25.908" size="1.27" layer="100">Vérifié par:</text>
<text x="0.508" y="18.288" size="1.27" layer="100">Approuvé par:</text>
<text x="0.508" y="10.668" size="1.27" layer="100">Format</text>
<text x="8.89" y="10.668" size="1.27" layer="100">Version</text>
<text x="19.05" y="10.668" size="1.27" layer="100">Révision</text>
<text x="28.448" y="18.542" size="1.27" layer="100">Chemin et nom du design:</text>
<text x="28.448" y="10.922" size="1.27" layer="100">Titre du schématique:</text>
<text x="0.762" y="2.794" size="1.27" layer="100">Date:</text>
<text x="68.072" y="1.778" size="1.27" layer="100">Feuille(s)</text>
<text x="77.724" y="1.778" size="1.27" layer="100">de</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CARTOUCHE_TGÉ" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;Cartouche de TGÉ: &lt;/b&gt; Grandeur A, dimention 8 1/2 x 11 pouces, orientation portrait.
&lt;p&gt;
Les élèves doivent utiliser la commande &lt;b&gt;TEXT&lt;/b&gt; pour placer les valeurs aux champs appropriés.&lt;p&gt;
&lt;b&gt;Couche  100 Cartouche, ne pas oublier de créer cette couche!&lt;/b&gt;&lt;p&gt;
&lt;b&gt;Size par défaut: 0,07&lt;/b&gt; ou ajusté en conséquence si pas asez d'espace.&lt;p&gt;
&lt;b&gt;Ratio:&lt;/b&gt; 8%&lt;p&gt;
&lt;b&gt;Font:&lt;/b&gt; Vector.&lt;p&gt;
Voir la commande TEXT pour plus d'information.</description>
<gates>
<gate name="G$1" symbol="CADRE_FORMAT_A" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="DOCUMENTATION" x="191.516" y="0.254" addlevel="always"/>
</gates>
<devices>
<device name="1" package="CARTOUCHE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LV8548MC">
<packages>
<package name="SOIC10-1.0-5X4MM">
<wire x1="-1.768909375" y1="-2" x2="-2.5" y2="-1.268909375" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.268909375" x2="-2.5" y2="2" width="0.127" layer="21"/>
<circle x="-2.921" y="-2.032" radius="0.3302" width="0" layer="21"/>
<text x="-3.17643125" y="-1.905859375" size="0.8894" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="0.635303125" y="-1.905909375" size="0.635303125" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.54198125" y1="-2.03358125" x2="2.54" y2="2.032" layer="39"/>
<wire x1="-2.5" y1="2" x2="-2.4" y2="2" width="0.127" layer="21"/>
<smd name="1" x="-2" y="-2.904" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-1" y="-2.904" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0" y="-2.904" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="1" y="-2.904" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="2" y="-2.904" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="2" y="2.777" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="1" y="2.777" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="0" y="2.777" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="9" x="-1" y="2.777" dx="1.905" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="-2" y="2.777" dx="1.905" dy="0.6" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="DRIVER-LV8548MC-AH">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="INT3" x="-15.24" y="-7.62" length="middle"/>
<pin name="OUT2" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="OUT3" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="OUT4" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="INT2" x="-15.24" y="-2.54" length="middle"/>
<pin name="OUT1" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="VCC" x="-15.24" y="7.62" length="middle" function="dotclk"/>
<pin name="INT1" x="-15.24" y="2.54" length="middle"/>
<pin name="INT4" x="-15.24" y="-12.7" length="middle"/>
<pin name="GND" x="15.24" y="-12.7" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DRIVER-LV8548MC-AH(SOIC10)" prefix="U">
<description>310030259</description>
<gates>
<gate name="G$1" symbol="DRIVER-LV8548MC-AH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC10-1.0-5X4MM">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="INT1" pad="2"/>
<connect gate="G$1" pin="INT2" pad="3"/>
<connect gate="G$1" pin="INT3" pad="4"/>
<connect gate="G$1" pin="INT4" pad="5"/>
<connect gate="G$1" pin="OUT1" pad="10"/>
<connect gate="G$1" pin="OUT2" pad="9"/>
<connect gate="G$1" pin="OUT3" pad="8"/>
<connect gate="G$1" pin="OUT4" pad="7"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" LV8548MC Series 16 V 1 A 1 W Dual Ch Forward/Reverse Motor Driver - MFP-10S "/>
<attribute name="MF" value="ON Semiconductor"/>
<attribute name="MP" value="LV8548MC-AH"/>
<attribute name="PACKAGE" value="SOIC-10 ON Semiconductor"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="1mylib">
<packages>
<package name="1X05">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-6.4262" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
</package>
<package name="1X05/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.985" x2="-5.08" y2="1.27" width="0.762" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.985" x2="5.08" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-6.985" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="8.255" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.461" y1="0.635" x2="-4.699" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="-5.461" y1="-2.921" x2="-4.699" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="4.699" y1="-2.921" x2="5.461" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD5">
<wire x1="-6.35" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PE">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.6223" y1="-1.016" x2="0.6223" y2="-1.016" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="PE" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X5" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X05">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X05/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;GND&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="PE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="1_TGE-sur G" deviceset="CARTOUCHE_TGÉ" device="1"/>
<part name="U1" library="LV8548MC" deviceset="DRIVER-LV8548MC-AH(SOIC10)" device=""/>
<part name="J1" library="1mylib" deviceset="PINHD-1X5" device=""/>
<part name="J2" library="1mylib" deviceset="PINHD-1X5" device=""/>
<part name="GND1" library="1mylib" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="81.28" y="-86.36" size="1.778" layer="100">projet/circuit/sch/bob-h-bridge.sch</text>
<text x="73.66" y="-93.98" size="1.778" layer="100">1</text>
<text x="132.842" y="-99.822" size="1.778" layer="100">1</text>
<text x="127.508" y="-99.822" size="1.778" layer="100">1</text>
<text x="53.34" y="-93.98" size="1.778" layer="100">A4</text>
<text x="63.5" y="-93.98" size="1.778" layer="100">1</text>
<text x="53.34" y="-71.12" size="1.778" layer="100">Zakary Kamal Ismail</text>
<text x="81.28" y="-68.58" size="1.778" layer="100">Projet d'intégration de TSO</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="-139.7" y="-101.6" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="51.816" y="-101.346" smashed="yes">
<attribute name="LAST_DATE_TIME" x="59.182" y="-99.568" size="2.54" layer="100" font="vector"/>
<attribute name="DRAWING_NAME" x="81.026" y="-94.488" size="2.54" layer="100" font="vector"/>
</instance>
<instance part="U1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="J1" gate="A" x="-30.48" y="-2.54" smashed="yes" rot="MR0">
<attribute name="NAME" x="-24.13" y="5.715" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-24.13" y="-12.7" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="J2" gate="A" x="30.48" y="-2.54" smashed="yes">
<attribute name="NAME" x="24.13" y="5.715" size="1.778" layer="95"/>
<attribute name="VALUE" x="24.13" y="-12.7" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="GND" x="20.32" y="-17.78" smashed="yes">
<attribute name="VALUE" x="17.78" y="-20.32" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="J1" gate="A" pin="1"/>
<wire x1="-27.94" y1="2.54" x2="-20.32" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="2.54" x2="-20.32" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="-20.32" y1="7.62" x2="-15.24" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="J1" gate="A" pin="2"/>
<wire x1="-17.78" y1="0" x2="-27.94" y2="0" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="0" x2="-17.78" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="INT1"/>
<wire x1="-17.78" y1="2.54" x2="-15.24" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="INT2"/>
<pinref part="J1" gate="A" pin="3"/>
<wire x1="-15.24" y1="-2.54" x2="-27.94" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="OUT3"/>
<pinref part="J2" gate="A" pin="3"/>
<wire x1="15.24" y1="-2.54" x2="27.94" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="J2" gate="A" pin="2"/>
<wire x1="27.94" y1="0" x2="17.78" y2="0" width="0.1524" layer="91"/>
<wire x1="17.78" y1="0" x2="17.78" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT2"/>
<wire x1="17.78" y1="2.54" x2="15.24" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="OUT1"/>
<wire x1="15.24" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="91"/>
<wire x1="20.32" y1="7.62" x2="20.32" y2="2.54" width="0.1524" layer="91"/>
<pinref part="J2" gate="A" pin="1"/>
<wire x1="20.32" y1="2.54" x2="27.94" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="J2" gate="A" pin="4"/>
<wire x1="27.94" y1="-5.08" x2="17.78" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-5.08" x2="17.78" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT4"/>
<wire x1="17.78" y1="-7.62" x2="15.24" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="J1" gate="A" pin="5"/>
<wire x1="-27.94" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-7.62" x2="-20.32" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="INT4"/>
<wire x1="-20.32" y1="-12.7" x2="-15.24" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="INT3"/>
<wire x1="-15.24" y1="-7.62" x2="-17.78" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-7.62" x2="-17.78" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="J1" gate="A" pin="4"/>
<wire x1="-17.78" y1="-5.08" x2="-27.94" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PE" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="-12.7" x2="20.32" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-12.7" x2="20.32" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="J2" gate="A" pin="5"/>
<wire x1="20.32" y1="-7.62" x2="27.94" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="GND1" gate="GND" pin="PE"/>
<wire x1="20.32" y1="-12.7" x2="20.32" y2="-15.24" width="0.1524" layer="91"/>
<junction x="20.32" y="-12.7"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
